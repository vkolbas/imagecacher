//
//  ViewController.swift
//  ImageCacher
//
//  Created by Vladimir Kolbas on 6/10/15.
//  Copyright (c) 2015 Vladimir Kolbas. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func getImage(sender: AnyObject) {
        
        imageView.dc_cachedImageForURLString("http://www.psdgraphics.com/file/colorful-triangles-background.jpg")
    }
}

