//
//  UIImageView+DCImageCache.swift
//  ImageCacher
//
//  Created by Vladimir Kolbas on 6/10/15.
//  Copyright (c) 2015 Vladimir Kolbas. All rights reserved.
//

//http://www.psdgraphics.com/file/colorful-triangles-background.jpg

import UIKit

extension UIImageView {
    
    static private let CompressionQuality : CGFloat     = 0.8
    typealias Completion                                = (UIImage?) -> Void
    
    func dc_cachedImageForURLString(url: String) {
        
        let strippedName = stripFilename(url)

        if let image = findCachedImage(strippedName) {
            self.image = image
        } else {
            println("No cached image, downloading")

            downloadImageAsync(url) { [unowned self] image in
                if let image = image {
                    println("Downloaded image")
                    self.saveCachedImage(image, name: strippedName)
                    self.image = image
                    println("Set image")
                }
            }
        }
        
    }
    
    
    
    
    // MARK: Helpers
    
    private func downloadImageAsync(urlString: String, completion: Completion) {
        if let url = NSURL(string: urlString) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
                if let data = NSData(contentsOfURL: url) {
                    if let image = UIImage(data: data) {
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            completion(image)
                        })
                    }
                }

            })
        }
        
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            completion(nil)
        })
    }
    
    private func urlForName(name: String) -> NSURL {
        let directory = NSFileManager.defaultManager().URLForDirectory(.CachesDirectory, inDomain: .UserDomainMask, appropriateForURL: nil, create: false, error: nil)!
        return directory.URLByAppendingPathExtension(name)
    }
    
    private func saveCachedImage(image: UIImage, name: String) {
        
        let path = urlForName(name)
        let imageData = UIImageJPEGRepresentation(image, self.dynamicType.CompressionQuality)
        let success = imageData.writeToURL(path, atomically: true)
        // File is saved for the next time, use cached for this session
        DCCache.sharedInstance.setImage(image, forKey: name)
    }
    
    private func findCachedImage(name: String) -> UIImage? {
        
        if let image = DCCache.sharedInstance.imageForKey(name) {
            println("Returning NScached image")
            return image
        }
        
        let imagePath = urlForName(name)

        if let pathString = imagePath.path {
            if NSFileManager.defaultManager().fileExistsAtPath(pathString) {
                if let image = UIImage(contentsOfFile: pathString) {
                    DCCache.sharedInstance.setImage(image, forKey: name)
                    println("Returning image from file, it will be in cache for the next time")
                    return image
                }
            }
        }
        
        return nil
    }
    
    private func stripFilename(name: String) -> String {
        let characterSet = NSCharacterSet(charactersInString: ":/")
        return (name.componentsSeparatedByCharactersInSet(characterSet)).reduce("", combine: { (a, b) -> String in
            a + b
        })
    }
    
    
}


