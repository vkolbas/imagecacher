//
//  DCCache.swift
//  ImageCacher
//
//  Created by Vladimir Kolbas on 6/10/15.
//  Copyright (c) 2015 Vladimir Kolbas. All rights reserved.
//

import UIKit

public class DCCache {

    static let sharedInstance   = DCCache()

    private let cache           = NSCache()
    
    public func imageForKey(name: String) -> UIImage? {
        return cache.objectForKey(name) as? UIImage
    }
    
    public func setImage(image: UIImage, forKey key: String) {
        cache.setObject(image, forKey: key)
    }
    
}