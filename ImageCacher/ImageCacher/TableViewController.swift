//
//  TableViewController.swift
//  ImageCacher
//
//  Created by Vladimir Kolbas on 6/10/15.
//  Copyright (c) 2015 Vladimir Kolbas. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 50
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! TestCell
        cell.testLabel.text = "Tst"
        cell.testImageView.dc_cachedImageForURLString("http://www.index.hr/img/2012/logo.png")
        
        return cell
    }
    
}

class TestCell: UITableViewCell {
    
    @IBOutlet var testImageView: UIImageView!
    @IBOutlet var testLabel: UILabel!
    
}

